import java.util.Random;

public class Paciente {
		
	   private String nombre, apellido, sexo, edad, peso, altura;
	   
	   

	public Paciente() {
 this ("","","","");
	}




	public Paciente(String nombre, String apellido, String sexo, String edad) {
		//super();
		this.setNombre(nombre);
		this.setApellido(apellido);
		this.setSexo(sexo);
		this.setEdad(edad);
		this.setPeso(this.autogenerarPeso());
		this.setAltura(this.autogenerarAltura());
	}




	public Paciente(String nombre, String apellido, String sexo, String edad, String peso, String altura) {
		super();
		this.setNombre(nombre);
		this.setApellido(apellido);
		this.setSexo(sexo);
		this.setEdad(edad);
		this.setPeso(peso);
		this.setAltura(altura);
	}



	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getEdad() {
		return edad;
	}

	public void setEdad(String edad) {
		this.edad = edad;
	}

	@Override
	public String toString() {
		return "Paciente [nombre=" + nombre + ", apellido=" + apellido + ", sexo=" + sexo + ", edad=" + edad + ", peso="+ peso + ", altura=" + altura + " ]";
	}
	   
	public String getPeso() {
		return peso;
	}

	public void setPeso(String peso) {
		this.peso = peso;
	}

	public String getAltura() {
		return altura;
	}

	public void setAltura(String altura) {
		this.altura = altura;
	}
	   
	private String autogenerarPeso() {
		Random rnd = new Random();
		return "" +(rnd.nextInt(10 - 5)+ 5) + (float) (rnd.nextInt(100) * 0.1);
	}

	private String autogenerarAltura() {
		Random rnd = new Random();
			return "" +((float) rnd.nextInt(2 - 1)+1+ (rnd.nextInt(10) *0.1));
		}
	

	
}
