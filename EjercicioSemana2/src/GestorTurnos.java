import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.NoSuchElementException;
import java.util.Scanner;

import javax.swing.JOptionPane;

public class GestorTurnos {

	   
   public String agregarPaciente(Paciente paciente) { 
	   ArrayList<Paciente> vecpacientes= descargaArchivo();
  
	   vecpacientes.add(paciente);
	     try(Formatter output = new Formatter("Turnos/Pacientes.txt")){
	    	 for (Paciente pac : vecpacientes) {
	    		 try {
		    		 output.format("%s %s %s %s %s %s %n", pac.getNombre(), pac.getApellido(), pac.getSexo(), pac.getEdad(), pac.getPeso(), pac.getAltura());
		    		 }	 
		    		 catch(NoSuchElementException elementException) {
		    			 System.err.print("Error");
		   			}
			} }
	     catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		   return "Paciente Agregado."; 
	     }
	
    private ArrayList<Paciente> descargaArchivo(){
    	ArrayList<Paciente> vecpacientes= new ArrayList<Paciente>();
    	try(Scanner consola= new Scanner(Paths.get("Turnos/Pacientes.txt"))){

            while (consola.hasNext()) {
            Paciente pac = new Paciente(consola.next(),consola.next(),consola.next(),consola.next(),consola.next(),consola.next());
            
            vecpacientes.add(pac);
            }
        } catch (IOException e) {//exception

            e.printStackTrace();
        }
    	return vecpacientes;
    }
  /*public void muestroLista(Paciente paciente) {
	   for (Paciente paciente : vecpacientes) {
		
	   
	   
   
	
   public void asignaTurnos() {
	  Paciente paciente;
	  JOptionPane.showInputDialog(null, "Desea asignar turno a un paciente?");
	  
  }*/
	
}

