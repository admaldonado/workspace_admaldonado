import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.time.LocalDate;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Main {

	public static void main(String[] args) {
		
		DetallesVenta venta = new DetallesVenta("Botella de agua", 20);
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(LocalDate.class, new LocalDateSerializer());
		gsonBuilder.registerTypeAdapter(LocalDate.class, new LocalDateDeserializer());
		Gson gson = gsonBuilder.setPrettyPrinting().create();
		
		//Gson gson= new Gson()
		String json = gson.toJson(venta);
		
		System.out.println(json);
		
        DetallesVenta venta2 = gson.fromJson(json, DetallesVenta.class);
        System.out.println(venta.getProducto());
					
			
			URL url;
			try {
		    //asigno URL
			url = new URL("https://raw.githubusercontent.com/alevega/datosCambio/master/datos.json");
			//abro conexion a URL
			URLConnection conexion = url.openConnection();
			//leer contenido de pagina web
			BufferedReader lectura = new BufferedReader(new InputStreamReader(conexion.getInputStream())); 
			
			String datos = "";
			String linea;
			while ((linea = lectura.readLine()) != null) {
				datos = datos+linea;
			}
			
			System.out.println(datos);
			
			Cambio cam = gson.fromJson(datos, Cambio.class);
			
			cam.impPromedio();
			cam.impFechas(LocalDate.of(2018, 7, 01), LocalDate.of(2018, 7, 31));
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			
	}
}
