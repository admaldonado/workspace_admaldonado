public class DetallesVenta {
   
	private String producto;
	private Integer cantidad;
	
	public DetallesVenta(String producto, Integer cantidad) {
		super();
		this.setProducto(producto);
		this.setCantidad(cantidad);
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	
}
