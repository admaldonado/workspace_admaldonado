
public class ResumenBasico {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//comentario de linea simple
		/*comentario de
		 *  multiples lineas
		 */
       /** comentario tipo JAVADOC
        * 
        */
		//TIPOS PRIMITIVOS
		int numEntero = 8;
		double numFlotante = 2.3;
		boolean booleano = true;
		char caracter = 'A';
		
		int casteo= (int) 3.4;
		System.out.println("numero"+casteo);
		//OPERADORES LOGICOS
		if(numEntero == numFlotante) {
			System.out.println("los numeros son iguales");
		} else if(numEntero > numFlotante) {
			System.out.println("numero entero es mayor a flotante");
		}else {
			System.out.println("el entero es menor o igual al flotante");
		}
		/* igualdad: ==
		 * desigual o distinto: !=
		 * menor: <
		 * mayor: >
		 * menor o igual: <=
		 * mayor o igual: >=
		 */
		if (numEntero != numFlotante && numEntero > 7) {
			System.out.println("el numero es distinto al flotante y es mayor a 7");
		}
		/* &: y
		 * |: o
		 * &&: y, no evaluando la segunda condicion si la primera es falsa
		 * ||: o, no evaluando la segunda condicion si la primera es verdadera
		 * ^: xor
		 */
		System.out.println(numEntero > numFlotante? "entero flotante" : "entero no flotante");
		
		int resultado = numEntero != 0 ? 100/numEntero : 0;
		
		switch(caracter) {
		case 'A': 
			System.out.println("el valor del caracter es A");
			break;
			case 'B':
				System.out.println("el valor del caracter es B");
		default:
			throw new IllegalArgumentException("unexpected value: "+ caracter);
		}
		//BUCLES
		int[] numerosEnteros = {1, 3, 5, 2, 6, 8, 7};
		
		int i = 0;
		while (i< numerosEnteros.length) {
			System.out.println(numerosEnteros[i]);
			i ++;
		}
		int j = 6;
		do {
			numerosEnteros[j] = j;
			System.out.println("num en do while: "+numerosEnteros[j]);
			j--;
		}
		while (j >=0);
		
		for (int k=0; k < numerosEnteros.length; k++) {
			System.out.println("en for: "+numerosEnteros[k]);
		}
		//OPERADORES DE ASIGNACION COMPUESTOS
		
		i+=7; //igual a i = i + 7
		i-=4;
		i*=5;
		i/=3;
		i&=9;
		
		++i; //incrementa en uno y usa el valor
		i++; //usa el valor y despues incrementa en uno
		
	}

}
